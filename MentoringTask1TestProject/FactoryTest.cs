﻿using System.IO;
using MentoringTask1.Enums;
using MentoringTask1.Factory;
using MentoringTask1.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MentoringTask1TestProject
{
	[TestClass]
	public class FactoryTest
	{
		private DirectoryInfo _directoryInfo;
		private string _objectName;
		private IObjectsFactory _objectsFactory;

		[TestInitialize]
		public void SetupTestData()
		{
			_directoryInfo = new DirectoryInfo("c:\\");
			_objectName = "Test_Object";
			_objectsFactory = new ObjectFactory();
		}

		[TestMethod]
		public void ShouldCreateNewObjectWithTypeDisk()
		{
			var obj = _objectsFactory.CreateNewDiskObject(_objectName, _directoryInfo);
			Assert.AreEqual(obj.ObjectType, ObjectType.Disk);
		}

		[TestMethod]
		public void ShouldCreateNewObjectWithTypeFile()
		{
			var obj = _objectsFactory.CreateNewFileObject(_objectName, _directoryInfo);
			Assert.AreEqual(obj.ObjectType, ObjectType.File);
		}

		[TestMethod]
		public void ShouldCreateNewObjectWithTypeFolder()
		{
			var obj = _objectsFactory.CreateNewFolderObject(_objectName, _directoryInfo);
			Assert.AreEqual(obj.ObjectType, ObjectType.Folder);
		}
	}
}
