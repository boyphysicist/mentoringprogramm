﻿using System;
using MentoringTask1.Interfaces;
using MentoringTask1.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MentoringTask1TestProject
{
	[TestClass]
	public class SystemVisitorTest
	{

		private IFileSystemVisitor _fileSystemVisitor;
		private Func<ISystemObject, bool> _sortFunc;

		[TestInitialize]
		public void SetupTestData()
		{
			_fileSystemVisitor = new FileSystemVisitors();
			_sortFunc = (res) => res.Name.Contains("test");
		}

		[TestMethod]
		public void AllNamesShouldConteinString()
		{
			foreach (var obj in _fileSystemVisitor.GetFilterdData(_sortFunc, @"..\..\testFolder"))
			{
				Assert.IsTrue(obj.Name.ToLower().Contains("test"));
			}
		}
	}
}
