﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntParser
{
	public static class Converter
	{
		private static int CharToInt(char c)
		{
			return c - '0';
		}

		public static int ToInt(string s)
		{
			bool isNegative = false;
			int start = 0;
			int result = 0;

			if (s == null) throw new ArgumentException("Null!!!!!!!!!!!!!!");
			if (s.Length == 0) throw new ArgumentException("Empty!!!!!!!!!!!");
			switch (s[0])
			{
				case '-':
					if (s.Length == 1) throw new ArgumentException("Must have value!!!");
					start = 1;
					isNegative = true;
					break;
				case '+':
					if (s.Length == 1) throw new ArgumentException("Must have value!!!");
					start = 1;
					break;
			}
			
			for (int i = start; i < s.Length; i++)
			{
				var c = s[i];

				if (c < '0' || c > '9') throw new ArgumentException();
				result = checked(result * 10 + CharToInt(s[i]));
			}
			return isNegative ? -result : result;
		}
	}
}
