﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Linq;
using SampleSupport;
using Task.Data;

// Version Mad01

namespace SampleQueries
{
	[Title("LINQ Module")]
	[Prefix("Linq")]
	public class LinqSamples : SampleHarness
	{

		private DataSource dataSource = new DataSource();

		[Category("Restriction Operators")]
		[Title("Where - Task 1")]
		[Description("This sample uses the where clause to find all elements of an array with a value less than 5.")]
		public void Linq1()
		{
			int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

			var lowNums =
				from num in numbers
				where num < 5
				select num;

			Console.WriteLine("Numbers < 5:");
			foreach (var x in lowNums)
			{
				Console.WriteLine(x);
			}
		}

		[Category("Restriction Operators")]
		[Title("Where - Task 2")]
		[Description("This sample return return all presented in market products")]

		public void Linq2()
		{
			var products =
				from p in dataSource.Products
				where p.UnitsInStock > 0
				select p;

			foreach (var p in products)
			{
				ObjectDumper.Write(p);
			}
		}

		[Category("Mentorin Task")]
		[Title("Task 1")]
		[Description("Выдайте список всех клиентов, чей суммарный оборот (сумма всех заказов) превосходит некоторую величину X. Продемонстрируйте выполнение запроса с различными X (подумайте, можно ли обойтись без копирования запроса несколько раз)")]
		public void Linq001()
		{
			var sums = new decimal[] {100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200};

			foreach (var sum in sums)
			{
				var clients = dataSource.Customers.Where(w => w.Orders.Select(s => s.Total).Sum() < sum);

				foreach (var client in clients)
				{
					Console.WriteLine(client.CompanyName);
				}

				Console.WriteLine(@"End");
			}
		}
		[Category("Mentorin Task")]
		[Title("Task 2")]
		[Description("Для каждого клиента составьте список поставщиков, находящихся в той же стране и том же городе. Сделайте задания с использованием группировки и без.")]
		public void Linq002()
		{
			var clients = dataSource.Customers;

			var suplies = dataSource.Suppliers;

			var dict = new Dictionary<Customer,IEnumerable<Supplier>>();
			var grDict1 = new Dictionary<Customer, IEnumerable<Supplier>>();
			foreach (var client in clients)
			{
				dict.Add(client, suplies.Where(w=>w.Country == client.Country && w.City == client.City));
				
				grDict1.Add(client, suplies.GroupBy(gr => gr.Country).Where(w => w.Key == client.Country).SelectMany(s => s.Where(wh => wh.City == client.City).Select(se => se)));
			}

			foreach (var key in dict.Keys)
			{
				var a = dict[key];

				
				Console.WriteLine(key.Country + " " + key.City);
				Console.WriteLine("___________________________");
				foreach (var b in a)
				{
					Console.WriteLine(b.Country + " " + b.City);
				}

				Console.WriteLine("___________________________");
			}

			Console.WriteLine("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");


			foreach (var key in grDict1.Keys)
			{
				var a = grDict1[key];

				
				Console.WriteLine(key.Country+" "+ key.City);
				Console.WriteLine("___________________________");
				foreach (var b in a)
				{
					Console.WriteLine(b.Country + " " + b.City);
				}

				Console.WriteLine("___________________________");
			}

			Console.WriteLine(dict.Keys.Count);
			Console.WriteLine(grDict1.Keys.Count);
		}

		[Category("Mentorin Task")]
		[Title("Task 3")]
		[Description(
			"Найдите всех клиентов, у которых были заказы, превосходящие по сумме величину X")]
		public void Linq003()
		{
			var clients = dataSource.Customers;

			var value = 10000;

			foreach (var client in clients.Where(w => w.Orders.Any(wh => wh.Total > value)))
			{
				Console.WriteLine(client.CustomerID);
			}
		}

		[Category("Mentorin Task")]
		[Title("Task 4")]
		[Description(
			"Выдайте список клиентов с указанием, начиная с какого месяца какого года они стали клиентами (принять за таковые месяц и год самого первого заказа)")]
		public void Linq004()
		{
			var clients = dataSource.Customers.Where(w=>w.Orders.Any());

			foreach (var customer in clients)
			{
				var date = customer.Orders.Select(s => s.OrderDate).Min();
				Console.WriteLine(@"{0} {1} {2}", customer.CustomerID, date.Month, date.Year);
			}
		}

		[Category("Mentorin Task")]
		[Title("Task 5")]
		[Description(
			"Сделайте предыдущее задание, но выдайте список отсортированным по году, месяцу, оборотам клиента (от максимального к минимальному) и имени клиента")]
		public void Linq005()
		{
			var clients = dataSource.Customers;
			var bbb = clients.Where(w => w.Orders.Any()).OrderBy(o => o.CompanyName)
				.ThenBy(t => t.Orders.OrderByDescending(th => th.Total).ThenBy(or => or.OrderDate));

			foreach (var customer in bbb)
			{
				var date = customer.Orders.Select(s => s.OrderDate).Min();
				Console.WriteLine(@"{0} {1} {2} {3}", customer.CustomerID, date.Month, date.Year, customer.Orders.Select(s=>s.Total).Sum());
			}
		}

		[Category("Mentorin Task")]
		[Title("Task 6")]
		[Description(
			"Укажите всех клиентов, у которых указан нецифровой почтовый код или не заполнен регион или в телефоне не указан код оператора (для простоты считаем, что это равнозначно «нет круглых скобочек в начале»).")]
		public void Linq006()
		{
			var clients = dataSource.Customers;
			int number;

			foreach (var client in clients.Where(w=>!int.TryParse(w.PostalCode, out number)||string.IsNullOrEmpty(w.Region)||!w.Phone.Contains("(")))
			{
				Console.WriteLine(client.CustomerID);
			}
		}

		[Category("Mentorin Task")]
		[Title("Task 7")]
		[Description(
			"Сгруппируйте все продукты по категориям, внутри – по наличию на складе, внутри последней группы отсортируйте по стоимости")]
		public void Linq007()
		{
			var result = dataSource
				.Products
				.GroupBy(g => g.Category)
				.Select(sel => new
				{
					sel.Key,
					Objects = sel.ToList()
						.GroupBy(g => g.UnitsInStock)
						.Select(s => new
						{
							s.Key,
							Objects = s.ToList()
								.GroupBy(g => g.UnitPrice)

						})
				});
		}

		[Category("Mentorin Task")]
		[Title("Task 8")]
		[Description(
			"Сгруппируйте товары по группам «дешевые», «средняя цена», «дорогие». Границы каждой группы задайте сами")]
		public void Linq008()
		{
			var a = new {low = 100, mid = 500, hi = 1000};
			var result = dataSource
				.Products.GroupBy(g => g.UnitPrice).ToList();

			var groupLow = result.Where(w => w.Key < a.low);
			var groupMid = result.Where(w => w.Key > a.low && w.Key < a.hi);
			var groupHi = result.Where(w => w.Key > a.hi);

			Console.WriteLine(groupLow.Count());
			Console.WriteLine(groupMid.Count());
			Console.WriteLine(groupHi.Count());

		}

		[Category("Mentorin Task")]
		[Title("Task 9")]
		[Description(
			"Рассчитайте среднюю прибыльность каждого города (среднюю сумму заказа по всем клиентам из данного города) и " +
			"среднюю интенсивность (среднее количество заказов, приходящееся на клиента из каждого города)")]
		public void Linq009()
		{
			var cityGr = dataSource.Customers.GroupBy(g=>g.City);

			foreach (var city in cityGr)
			{
				var customers = city.ToList();
				if (customers.Count > 0)
				{ 
				var resultSumm = customers.Select(s =>
				{
					Order[] orders = s.Orders;
					return s.Orders.Length > 0
						? s.Orders.Select(sel => sel.Total).Sum()
						: 0 / (orders?.Length != 0 ? s.Orders.Length : 1);
				})
										  .Sum()/customers.Count;
				var resultCount = customers.Select(s => s.Orders.Length).Sum()/customers.Count;

				Console.WriteLine(resultSumm);
				Console.WriteLine(resultCount);
				}
			}
		}

		[Category("Mentorin Task")]
		[Title("Task 10")]
		[Description(
			"Рассчитайте среднюю прибыльность каждого города (среднюю сумму заказа по всем клиентам из данного города) и " +
			"среднюю интенсивность (среднее количество заказов, приходящееся на клиента из каждого города)")]
		public void Linq010()
		{
			var orders = dataSource.Customers.SelectMany(s => s.Orders);
			var orderMonthGr = orders.GroupBy(gr=>gr.OrderDate.Month);
			var orderYearGr = orders.GroupBy(gr => gr.OrderDate.Year);

			var orderYearMonth = orderYearGr.Select(s => new{ s.Key, Month = s.ToList().GroupBy(gr => gr.OrderDate.Month)});

			foreach (var month in orderMonthGr)
			{
				Console.WriteLine(month.Count());
			}

			foreach (var year in orderYearGr)
			{
				Console.WriteLine(year.Count());
			}

			foreach (var obj in orderYearMonth)
			{
				Console.WriteLine(obj.Key);
			}
		}
	}
}
