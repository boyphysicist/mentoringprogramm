﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task.Data
{
	public class Supplier:IComparable<Supplier>
	{
		public string SupplierName { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string Country { get; set; }

		public int CompareTo(Supplier other)
		{
			if (ReferenceEquals(this, other)) return 0;
			if (ReferenceEquals(null, other)) return 1;
			var supplierNameComparison = string.Compare(SupplierName, other.SupplierName, StringComparison.Ordinal);
			if (supplierNameComparison != 0) return supplierNameComparison;
			var addressComparison = string.Compare(Address, other.Address, StringComparison.Ordinal);
			if (addressComparison != 0) return addressComparison;
			var cityComparison = string.Compare(City, other.City, StringComparison.Ordinal);
			if (cityComparison != 0) return cityComparison;
			return string.Compare(Country, other.Country, StringComparison.Ordinal);
		}
	}
}
