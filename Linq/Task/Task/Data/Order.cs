﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task.Data
{
	public class Order:IComparable<Order>
	{
		public int OrderID { get; set; }
		public DateTime OrderDate { get; set; }
		public decimal Total { get; set; }

		public int CompareTo(Order other)
		{
			if (ReferenceEquals(this, other)) return 0;
			if (ReferenceEquals(null, other)) return 1;
			var orderIdComparison = OrderID.CompareTo(other.OrderID);
			if (orderIdComparison != 0) return orderIdComparison;
			var orderDateComparison = OrderDate.CompareTo(other.OrderDate);
			if (orderDateComparison != 0) return orderDateComparison;
			return Total.CompareTo(other.Total);
		}
	}
}
