﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task.Data
{
	public class Product:IComparable<Product>
	{
		public int ProductID { get; set; }
		public string ProductName { get; set; }
		public string Category { get; set; }
		public decimal UnitPrice { get; set; }
		public int UnitsInStock { get; set; }

		public int CompareTo(Product other)
		{
			if (ReferenceEquals(this, other)) return 0;
			if (ReferenceEquals(null, other)) return 1;
			var productIdComparison = ProductID.CompareTo(other.ProductID);
			if (productIdComparison != 0) return productIdComparison;
			var productNameComparison = string.Compare(ProductName, other.ProductName, StringComparison.Ordinal);
			if (productNameComparison != 0) return productNameComparison;
			var categoryComparison = string.Compare(Category, other.Category, StringComparison.Ordinal);
			if (categoryComparison != 0) return categoryComparison;
			var unitPriceComparison = UnitPrice.CompareTo(other.UnitPrice);
			if (unitPriceComparison != 0) return unitPriceComparison;
			return UnitsInStock.CompareTo(other.UnitsInStock);
		}
	}
}
