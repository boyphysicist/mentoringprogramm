﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task.Data
{
	public class Customer:IComparable<Customer>
	{
		public string CustomerID { get; set; }
		public string CompanyName { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string Region { get; set; }
		public string PostalCode { get; set; }
		public string Country { get; set; }
		public string Phone { get; set; }
		public string Fax { get; set; }
		public Order[] Orders { get; set; }

		public int CompareTo(Customer other)
		{
			if (ReferenceEquals(this, other)) return 0;
			if (ReferenceEquals(null, other)) return 1;
			var customerIdComparison = string.Compare(CustomerID, other.CustomerID, StringComparison.Ordinal);
			if (customerIdComparison != 0) return customerIdComparison;
			var companyNameComparison = string.Compare(CompanyName, other.CompanyName, StringComparison.Ordinal);
			if (companyNameComparison != 0) return companyNameComparison;
			var addressComparison = string.Compare(Address, other.Address, StringComparison.Ordinal);
			if (addressComparison != 0) return addressComparison;
			var cityComparison = string.Compare(City, other.City, StringComparison.Ordinal);
			if (cityComparison != 0) return cityComparison;
			var regionComparison = string.Compare(Region, other.Region, StringComparison.Ordinal);
			if (regionComparison != 0) return regionComparison;
			var postalCodeComparison = string.Compare(PostalCode, other.PostalCode, StringComparison.Ordinal);
			if (postalCodeComparison != 0) return postalCodeComparison;
			var countryComparison = string.Compare(Country, other.Country, StringComparison.Ordinal);
			if (countryComparison != 0) return countryComparison;
			var phoneComparison = string.Compare(Phone, other.Phone, StringComparison.Ordinal);
			if (phoneComparison != 0) return phoneComparison;
			return string.Compare(Fax, other.Fax, StringComparison.Ordinal);
		}
	}
}
