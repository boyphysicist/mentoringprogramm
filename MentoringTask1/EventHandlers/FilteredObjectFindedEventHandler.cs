﻿using MentoringTask1.EventArgs;

namespace MentoringTask1.EventHandlers
{
	public delegate void FilteredObjectFindedEventHandler(object sender, ObjectFindedEventArgs arg);
}
