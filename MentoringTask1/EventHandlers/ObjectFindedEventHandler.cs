﻿using MentoringTask1.EventArgs;

namespace MentoringTask1.EventHandlers
{
	public delegate void ObjectFindedEventHandler(object sender, ObjectFindedEventArgs arg);
}
