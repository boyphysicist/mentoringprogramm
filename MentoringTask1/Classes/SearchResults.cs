﻿using MentoringTask1.Enums;
using MentoringTask1.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace MentoringTask1.Classes
{
	public class SearchResults
	{
		private List<ISystemObject> _systemObjects;

		public SearchResults()
		{
			_systemObjects = new List<ISystemObject>();
		}

		public int Length => _systemObjects.Count;

		public ISystemObject this[int index]
		{
			get => _systemObjects[index];
			set => _systemObjects[index] = value;
		}

		public void AddObject(ISystemObject systemObject)
		{
			_systemObjects.Add(systemObject);
		}

		public void AddRange(IEnumerable<ISystemObject> systemObjects)
		{
			_systemObjects.AddRange(systemObjects);
		}

		public IEnumerable<ISystemObject> GetObjects(string key)
		{
			return _systemObjects.Where(w => w.Name.Contains(key));
		}

		public IEnumerable<ISystemObject> GetObjects(int maxCount)
		{
			for (int i = 0; i < maxCount; i++)
			{
				if (i == Length)
				{
					yield break;
				}

				yield return _systemObjects[i];
			}
		}

		public IEnumerable<ISystemObject> GetAllObjects() => _systemObjects;

		public IEnumerable<ISystemObject> GetDiskObjects() => _systemObjects.Where(w => w.ObjectType == ObjectType.Disk);
	}
}
