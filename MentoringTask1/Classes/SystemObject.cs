﻿using System.IO;
using MentoringTask1.Enums;
using MentoringTask1.Interfaces;

namespace MentoringTask1.Classes
{
	public class SystemObject : ISystemObject
	{
		public string Name { get; }

		public DirectoryInfo RootDir { get; }

		public ObjectType ObjectType { get; }

		public SystemObject ( string name, DirectoryInfo rootDir, ObjectType objectType)
		{
			Name = name;
			RootDir = rootDir;
			ObjectType = objectType;
		}
	}
}
