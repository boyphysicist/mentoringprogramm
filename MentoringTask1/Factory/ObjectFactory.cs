﻿using System;
using System.IO;
using MentoringTask1.Classes;
using MentoringTask1.Enums;
using MentoringTask1.EventArgs;
using MentoringTask1.EventHandlers;
using MentoringTask1.Interfaces;

namespace MentoringTask1.Factory
{
	public class ObjectFactory:IObjectsFactory
	{
		public ISystemObject CreateNewDiskObject( string name, DirectoryInfo rootDir)
		{
			return new SystemObject(name, rootDir, ObjectType.Disk);
		}

		public ISystemObject CreateNewFileObject(string name, DirectoryInfo rootDir)
		{
			return new SystemObject(name, rootDir, ObjectType.File);
		}

		public ISystemObject CreateNewFolderObject(string name, DirectoryInfo rootDir)
		{
			return new SystemObject(name, rootDir, ObjectType.Folder);
		}
	}
}
