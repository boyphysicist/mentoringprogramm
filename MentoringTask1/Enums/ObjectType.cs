﻿namespace MentoringTask1.Enums
{
	public enum ObjectType
	{
		Disk,
		Folder,
		File
	}
}
