﻿using System;
using System.Collections.Generic;

namespace MentoringTask1.Interfaces
{
	public interface IFileSystemVisitor
	{
		IEnumerable<ISystemObject> GetFilterdData(Func<ISystemObject, bool> filteringFunc, string path);
	}
}
