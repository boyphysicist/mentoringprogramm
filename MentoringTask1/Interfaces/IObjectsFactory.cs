﻿using System.IO;


namespace MentoringTask1.Interfaces
{
	public interface IObjectsFactory
	{
		ISystemObject CreateNewDiskObject(string name, DirectoryInfo rootDir);
		ISystemObject CreateNewFileObject(string name, DirectoryInfo rootDir);
		ISystemObject CreateNewFolderObject(string name, DirectoryInfo rootDir);
	}
}
