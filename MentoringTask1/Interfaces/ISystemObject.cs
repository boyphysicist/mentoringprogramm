﻿using System.IO;
using MentoringTask1.Enums;

namespace MentoringTask1.Interfaces
{
	public interface ISystemObject
	{
		string Name { get;}
		DirectoryInfo RootDir { get;}
		ObjectType ObjectType { get; }
	}
}
