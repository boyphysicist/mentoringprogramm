﻿using System;
using MentoringTask1.EventArgs;
using MentoringTask1.EventHandlers;
using MentoringTask1.Interfaces;
using MentoringTask1.Services;

namespace MentoringTask1
{
	public class Program
	{
		static void Main(string[] args)
		{
			var fsv1 = new FileSystemVisitors();
			Func<ISystemObject, bool> sortFunc = (res) => res.Name.Contains("horizon");
			fsv1.ObjectFinded += OnObjectFinded;
			fsv1.FilteredObjectFinded += OnSortedObjectFinded;


			foreach (var systemObject in fsv1.GetFilterdData(sortFunc,"D:\\"))
			{
				Console.WriteLine(systemObject.Name);
			}
			
			Console.ReadLine();
		}

		private static void OnObjectFinded(object arg1, ObjectFindedEventArgs arg2)
		{
			arg2.NeedToIgnore = true;
		}

		private static void OnSortedObjectFinded(object arg1, ObjectFindedEventArgs arg2)
		{
			arg2.NeedToIgnore = false;
		}

	}
}
