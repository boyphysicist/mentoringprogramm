﻿using MentoringTask1.Enums;
using MentoringTask1.Interfaces;

namespace MentoringTask1.EventArgs
{
	public class ObjectFindedEventArgs
	{
		public ISystemObject SystemObject { get; set; }
		public bool NeedToStopSearch { get; set; }
		public bool NeedToIgnore { get; set; }
	}
}
