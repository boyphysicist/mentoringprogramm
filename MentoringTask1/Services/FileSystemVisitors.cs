﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MentoringTask1.Classes;
using MentoringTask1.Enums;
using MentoringTask1.EventArgs;
using MentoringTask1.EventHandlers;
using MentoringTask1.Factory;
using MentoringTask1.Interfaces;

namespace MentoringTask1.Services
{
	public class FileSystemVisitors : IFileSystemVisitor, IEnumerable<ISystemObject>
	{

		private Func<ISystemObject, bool> _filteringFunc;

		private readonly SearchResults _searchResults;
		private readonly IObjectsFactory _objectsFactory;

		public event ObjectFindedEventHandler ObjectFinded;
		public event FilteredObjectFindedEventHandler FilteredObjectFinded;

		private bool Stoped { get; set; }
		
		public FileSystemVisitors()
		{
			_searchResults = new SearchResults();
			_objectsFactory = new ObjectFactory();
			Stoped = true;
		}
		
		private void OnObjectFinded(object sender, ObjectFindedEventArgs arg)
		{
			if (arg.NeedToStopSearch) StopSearch();

			if(!arg.NeedToIgnore) _searchResults.AddObject(arg.SystemObject);
		}

		private void OnFilteredObjectFinded(object sender, ObjectFindedEventArgs arg)
		{
			if (arg.NeedToStopSearch)
			{
				StopSearch();
			}

			if (arg.NeedToIgnore)
			{
				return;
			}

			_searchResults.AddObject(arg.SystemObject);
		}

		private void StartSearch(string path)
		{
			if (!Stoped) return;

			Stoped = false;

			var firstObjectObjects = FindStartObject(path);

			FindAllFoldersAndFiles(firstObjectObjects, firstObjectObjects.RootDir);

			Stoped = true;
		}

		private void StopSearch()
		{
			ObjectFinded -= OnObjectFinded;
			Stoped = true;
		}

		public IEnumerable<ISystemObject> GetFilterdData(Func<ISystemObject, bool> filteringFunc, string path)
		{
			_filteringFunc = filteringFunc;

			StartSearch(path);


			return _searchResults.GetAllObjects();
		}

		private ISystemObject FindStartObject(string path)
		{
			var firstObjectInfo = new DirectoryInfo(path);

			var firstObject = _objectsFactory.CreateNewFolderObject(firstObjectInfo.Name, firstObjectInfo);
			var args = new ObjectFindedEventArgs
			{
				SystemObject = firstObject
			};
			if (_filteringFunc(firstObject))
			{
				FilteredObjectFinded?.Invoke(this, args);
				OnFilteredObjectFinded(this, args);
			}
			else
			{
				ObjectFinded?.Invoke(this, args);
				OnObjectFinded(this,args);
			}

			return firstObject;
		}

		private Status FindAllFoldersAndFiles(ISystemObject folderObject, DirectoryInfo directory)
		{
			while (!Stoped)
			{
				FileInfo[] files;

				try
				{
					files = directory.GetFiles(".");
				}
				catch (UnauthorizedAccessException)
				{
					return Status.NeOK;
				}
				catch (DirectoryNotFoundException)
				{
					return Status.NeOK;
				}

				foreach (var file in files)
				{
					var obj = _objectsFactory.CreateNewFileObject(file.Name, file.Directory);
					UseFilter(obj);
				}

				if (Stoped)
				{
					break;
				}

				try
				{
					var subDirs = directory.GetDirectories();

					var folderObjects = new List<ISystemObject>(subDirs.Length);

					folderObjects.AddRange(from dir in subDirs
										   let flObject = _objectsFactory.CreateNewFolderObject(dir.FullName, dir.Root)
										   let status = FindAllFoldersAndFiles(flObject, dir)
										   where status == Status.OK
										   select flObject);
					foreach (var folder in folderObjects)
					{
						UseFilter(folder);
					}
				}
				catch (UnauthorizedAccessException)
				{
					return Status.NeOK;
				}

				return Status.OK;
			}

			return Status.Stopped;
		}

		private void UseFilter(ISystemObject obj)
		{
			if (Stoped)
			{
				return;
			}

			var objectFindedEventArgs = new ObjectFindedEventArgs { SystemObject = obj };

			if (_filteringFunc(obj))
			{
				FilteredObjectFinded?.Invoke(this, objectFindedEventArgs);
				OnFilteredObjectFinded(this, objectFindedEventArgs);
			}
			else
			{
				ObjectFinded?.Invoke(this, objectFindedEventArgs);
				OnObjectFinded(this, objectFindedEventArgs);
			}
		}


		public IEnumerator<ISystemObject> GetEnumerator()
		{
			return _searchResults.GetAllObjects().GetEnumerator();
		}


		IEnumerator IEnumerable.GetEnumerator()
		{
			for (int i = 0; i < _searchResults.Length; i++)
			{
				yield return _searchResults[i];
			}
		}
	}
}