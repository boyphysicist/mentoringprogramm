﻿using System;
using MentoringTask_ExHandling_.Classes;

namespace MentoringTask_ExHandling_
{
	public static class Program
	{
		static void Main(string[] args)
		{
			StartParseStringToInt();

			Console.ReadLine();
		}


		private static void StartParseMass()
		{
			Console.WriteLine("Input string value:");

			try
			{
				var val = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

				if (val <= 0)
				{
					throw new InvalidOperationException("Must be greater than zero");
				}

				var res = new string[val];

				for (var i = 0; i < val; i++)
				{
					Console.WriteLine("Input string");
					res[i] = Console.ReadLine();
				}

				foreach (var str in StringParser.ParseStrings(res))
				{
					Console.WriteLine(str);
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message + " Try input valid value)");
				StartParseMass();
			}

		}

		private static void StartParseStringToInt()
		{
			try
			{
				Console.WriteLine(IntParser.Converter.ToInt(Console.ReadLine()).GetType());
			}
			catch (ArgumentException e)
			{
				Console.WriteLine(e.Message);
			}
			finally
			{
				StartParseStringToInt();
			}
		}
	}
}

