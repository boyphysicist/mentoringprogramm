﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MentoringTask_ExHandling_.Classes
{
	public static class StringParser
	{

		private static string Parse(string inputString)
		{
			string result;

			try
			{
				result = inputString[0].ToString();
			}
			catch (IndexOutOfRangeException e)
			{
				throw new ParseException("String is empty!!!");
			}

			return result;
		}

		public static string[] ParseStrings(string[] inputStrings)
		{
			var result = new string[inputStrings.Length];

			var i = 0;

			foreach (var inputString in inputStrings)
			{
				try
				{
					result[i] = Parse(inputString);
				}
				catch (ParseException e)
				{
					result[i] = e.Message;
				}
				finally
				{
					i += 1;
				}

			}

			return result;
		}

	}
}
